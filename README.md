# Minecraft Vanilla
This Yunohost package will make you able to install Minecraft Vanilla on your server.

## Requirements
* A root SSH access to your server
* A server based on Debian

## Installtion

### On YunoHost

```bash
sudo yunohost app install https://gitea.com/chopin42/vanilla_ynh
sudo yunohost firewall allow TCP 25565
sudo yunohost firewall allow TCP 25575
```

### On any Debian-based system

```bash
curl https://gitea.com/chopin42/vanilla_ynh/raw/branch/master/scripts/install | bash
```

## Change the settings of the game
If you already know how to change the settings of a Minecraft Server using `minecraft.propeties` and others.
You can do it into the folder `/home/yunohost.app/vanilla`.

Don't forget to run the following command to apply your changes: `systemctl restart vanilla`. 

## Manage your server

* `systemctl start vanilla` : Start the server
* `systemctl stop vanilla` : Stop the server
* `systemctl restart vanilla` : Restart the server
* `systemctl enable vanilla` : Makes the server auto-start when booting (default)
* `systemctl disable vanilla` : Do the opposite. 
* `systemctl status vanilla` : Check the status of the app

## Uninstall
### On YunoHost

```bash
sudo yunohost app remove vanilla
```

### On any Debian-based system

```bash
curl https://gitea.com/chopin42/vanilla_ynh/raw/branch/master/scripts/remove | bash
```

